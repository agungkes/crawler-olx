import fetch from 'node-fetch'
import { findByKey } from './helper'
import { OlxResponse } from './types'

interface SearchProps {
  categoryId: number | string
  locationId: number | string
  page: number | string
  query: string
}
const search = async (props: SearchProps) => {
  const res = await fetch(
    `https://www.olx.co.id/api/relevance/search?query=${props.query.replace(
      /\s/g,
      '+'
    )}&category=${props.categoryId}&facet_limit=100&location=${
      props.locationId
    }&location_facet_limit=20?page=${props.page}`
  )
  const json: OlxResponse = await res.json()

  if (json.data)
    return json.data.map(p => ({
      car_type: findByKey(p.parameters, 'm_body'),
      colour: findByKey(p.parameters, 'm_color'),
      condition: p.status.flags.new ? 'Baru' : 'Bekas',
      created: p.created_at,
      description: p.description,
      engine_capacity: findByKey(p.parameters, 'm_engine_capacity'),
      fuel: findByKey(p.parameters, 'm_fuel'),
      images: {
        big: p.images.map(i => i.big.url),
        full: p.images.map(i => i.full.url),
        medium: p.images.map(i => i.medium.url),
        small: p.images.map(i => i.small.url)
      },
      lastaccess: '',
      link: `https://www.olx.co.id/item/${p.title.replace(/\s/g, '-')}--iid-${
        p.id
      }`,
      location: {
        address: `${p.locations_resolved.SUBLOCALITY_LEVEL_1_name},${p.locations_resolved.ADMIN_LEVEL_3_name}`,
        city: p.locations_resolved.ADMIN_LEVEL_3_name,
        lat: p.locations[0].lat,
        long: p.locations[0].lon,
        province_name: p.locations_resolved.ADMIN_LEVEL_1_name
      },
      manufacture: findByKey(p.parameters, 'make'),
      marketplace: 'olx.co.id',
      mileage: findByKey(p.parameters, 'mileage'),
      model: findByKey(p.parameters, 'm_type'),
      price: {
        original: p.price.value.raw
      },
      stats: {
        favorited_count: p.favorites.count,
        item_count: 1
      },
      status: p.status.status,
      title: p.title,
      transmition: findByKey(p.parameters, 'm_transmission'),
      year: findByKey(p.parameters, 'm_year')
    }))

  return []
}

export default search
