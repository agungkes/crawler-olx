interface Data {
  formatted_value: string
  key: string
  key_name: string
  type: string
  value: string
  value_name: string
}
export const findByKey = (data: Data[], key: string) => {
  const keyType = data.find(d => d.key === key)
  if (keyType) return keyType.formatted_value

  return ''
}
